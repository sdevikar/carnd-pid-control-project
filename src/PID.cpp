#include "PID.h"
#include <iostream>
#include <valarray>


using namespace std;

/*
* TODO: Complete the PID class.
*/

PID::PID() {}

PID::~PID() {}

void PID::Init(double Kp, double Ki, double Kd) {
  //TODO Tune the following parameters
  std::cout << "Initializeed PID" << std::endl;
  this->Kp = Kp;
  this->Ki = Ki;
  this->Kd = Kd;
}

void PID::UpdateError(double cte, bool twiddle) {
  cte_sum += cte;

  p_error = Kp * cte;
  d_error = Kd * (cte - previous_cte);
  i_error = Ki * cte_sum;
  previous_cte = cte;
}

double PID::TotalError() {
  double total_error = -p_error - d_error - i_error;
  std::cout << "total error: " << total_error << endl;
  return (total_error);
}

double PID::CumulativeError(){
  return cte_sum;
}
